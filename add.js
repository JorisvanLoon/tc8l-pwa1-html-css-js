// Detects if device is on iOS 
const isIos = () => {
  const userAgent = window.navigator.userAgent.toLowerCase();
  return /iphone|ipad|ipod/.test( userAgent );
}
// Detects if device is in standalone mode
const isInStandaloneMode = () => ('standalone' in window.navigator) && (window.navigator.standalone);

// Checks if should display install popup notification:
if (isIos() && !isInStandaloneMode()) {
  var btn = document.createElement("BUTTON");
  btn.className = 'add_button';
    var t = document.createTextNode("Voeg me toe aan je homescreen!");
    btn.appendChild(t);
    document.body.appendChild(btn);
};

//Deze timeout haalt de knop weg na 5 seconden
setTimeout(function(){
  $(btn).remove();
}, 8000);