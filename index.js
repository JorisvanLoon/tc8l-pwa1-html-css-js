                        Survey.defaultBootstrapCss.navigationButton = "btn btn-green";
Survey.defaultBootstrapCss.rating.item = "btn btn-default my-rating";
Survey.StylesManager.applyTheme("bootstrap");



var json = { pages: [
        {questions: [
            { type: "text", name: "suggestions", title:"Welkom bij deze vragenlijst. Heb je een auto?",

             },
            { type: "rating", name: "satisfaction", title: "Hoe blij ben je met je auto?", 
                mininumRateDescription: "Niet blij", maximumRateDescription: "Heel blij" },
            { type: "rating", name: "recommend friends", visibleIf: "{satisfaction} > 3", 
                title: "Zou je je auto aan iemand anders willen aanbevelen?", 
                mininumRateDescription: "Will not recommend", maximumRateDescription: "I will recommend" },
            { type: "text", name: "sa", title:"What would make you more satisfied with the Product?",

             }
        ]},
        
        {questions: [
            { type: "radiogroup", name: "price to competitors", 
                title: "Compared to our competitors, do you feel the Product is",
                choices: ["Less expensive", "Priced about the same", "More expensive", "Not sure"]},
            { type: "radiogroup", name: "price", title: "Do you feel our current price is merited by our product?",
                choices: ["correct|Yes, the price is about right", 
                        "low|No, the price is too low for your product", 
                        "high|No, the price is too high for your product"]},
            { type: "multipletext", name: "pricelimit", title: "What is the... ",
                items: [{ name: "mostamount", title: "Most amount you would every pay for a product like ours" },
                        { name: "leastamount", title: "The least amount you would feel comfortable paying" }]}
        ]},
        { questions: [
                { type: "text", name: "email", 
                    title: "Thank you for taking our survey. Your survey is almost complete, please enter your email address in the box below if you wish to participate in our drawing, then press the 'Submit' button."}
            ]}
    ]
};

window.survey = new Survey.Model(json);


survey.onComplete.add(function(result) {
    document.querySelector('#surveyResult').innerHTML = "result: " + JSON.stringify(result.data);
});


$("#surveyElement").Survey({ 
    model: survey 
});

